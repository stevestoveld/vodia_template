#!/bin/bash

trap 'echo "unable to exit now, wait for build to finish..."'  SIGINT

# grab the wan ip of local machine
local_ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

# set the motd
cat > /etc/motd <<EOF
##########################################
##    CloudCo Partner Vodia Template    ##
##########################################

You can access the Vodia web GUI
at the following URL:

http://$local_ip
EOF

# check if server built already
if [ -f /home/vodiauser/.flag ]	# if the flag exists
then
	echo "server already built... exiting build script..."
	exit
else	# continue building

# vars
ansible_ip=23.90.82.114
ansible_pw="JydVbtl'&iQY^Y3wVM@\""
pubkey=`sshpass -p $ansible_pw ssh ansible@$ansible_ip cat /home/ansible/.ssh/id_rsa.pub`
pubkey=$pubkey
auth_keys=/home/ansible/.ssh/authorized_keys

# clear the screen
clear

# resize disk
echo "resizing disk /dev/xvda1..."
resize2fs /dev/xvda1
df -h
sleep 1

# set timezone
echo "setting timezone..."
dpkg-reconfigure tzdata
sleep 1

# update apt cache
echo "updating apt cache..."
apt-get update &> /dev/null
sleep 1

# add ansible user
echo "adding users..."
adduser ansible --gecos "ansible,,," --disabled-password &> /dev/null
echo "ansible:$ansible_pw" | chpasswd
adduser ansible sudo &> /dev/null
sleep 2

# generate ssh key to be used and set permissions
echo "generating ssh key on local machine..."
ssh-keygen -f /home/ansible/.ssh/id_rsa -t rsa -C ansible@$local_ip -N '' &> /dev/null
echo "setting permissions on public and private keys..."
chown ansible:ansible /home/ansible/.ssh/id_rsa*
chmod u+x /home/ansible/.ssh
sleep 1

# copy public ssh key from local machine to ansible host
echo "exchanging keys..."
sshpass -p $ansible_pw ssh-copy-id -i /home/ansible/.ssh/id_rsa.pub ansible@$ansible_ip &> /dev/null

if [ -f "$auth_keys" ]  # if authorized_keys file exists
then
        if grep -Fxq "$pubkey" $auth_keys       # if key already exists
        then
        	echo "key already exists... skipping..."
        else
        	echo "appending key to authorized_keys file..."
                echo $pubkey >> $auth_keys      # append key to file if absent
        fi
else
	echo "authorized_keys file does not exist... creating file and appending key..."
        echo $pubkey > $auth_keys       # create file and write key to file
fi
sleep 1

# set permissions on authorized_keys file
echo "setting permissions..."
chown ansible:ansible $auth_keys
chmod 600 $auth_keys
sleep 1

# pull down hosts file from ansible
sshpass -p $ansible_pw scp ansible@$ansible_ip:/etc/ansible/vodia_hosts /tmp/vodia_hosts

# add the ip if it doesn't exist
if grep -Fxq $local_ip /tmp/vodia_hosts
then
	echo "ip exists in hosts file... skipping..."
else
	echo "ip absent from hosts file... appending..."
	echo $local_ip >> /tmp/vodia_hosts
fi

# copy the new hosts file back to ansible
sshpass -p $ansible_pw scp /tmp/vodia_hosts ansible@$ansible_ip:/etc/ansible/vodia_hosts

# create flag
echo "done" > /home/vodiauser/.flag

# clean up
echo "tidying up..."

# lock root account
passwd --lock root &> /dev/null

# remove sudo permissions from vodiauser
deluser vodiauser sudo &> /dev/null
sed -i '/vodiauser/d' /etc/sudoers

# reboot the server to clear permissions and clean up
echo "server needs to reboot to finish setup..."
sleep 10

# self destruct
rm -- "$0"
reboot

fi
